from mpu6050 import mpu6050
import socket
import time
from math import sqrt,floor
from Adafruit_CharLCD import Adafruit_CharLCD
import time

import Adafruit_CharLCD as LCD



#Socket config
UDP_IP = "192.168.1.101"
print "Receiver IP: ", UDP_IP
UDP_PORT = int(5555)
print "Port: ", UDP_PORT
sock = socket.socket(socket.AF_INET, # Internet
                    socket.SOCK_DGRAM) # UDP
sock.bind((UDP_IP, UDP_PORT))


# Raspberry Pi pin configuration:
lcd_rs        = 26
lcd_en        = 19
lcd_d4        = 13
lcd_d5        = 6
lcd_d6        = 5
lcd_d7        = 11
lcd_backlight = 4
lcd_columns = 16
lcd_rows    = 2

lcd = LCD.Adafruit_CharLCD(lcd_rs, lcd_en, lcd_d4, lcd_d5, lcd_d6, lcd_d7,
                           lcd_columns, lcd_rows, lcd_backlight)







ux=0
uy=0
uz=0
t=.5
while(1):
        sensor = mpu6050(0x68)
        time.sleep(.5)
        ad=sensor.get_accel_data()
        vx=ux+floor(ad['x'])*t

        ux=vx
        print("speed=",abs(vx))
        data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes
        lcd.clear()
        lcd.message(data+"\nspeed= "+str((abs(vx))))
