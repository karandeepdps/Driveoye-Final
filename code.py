
import socket
from keras.layers import BatchNormalization, Flatten, Input, Dense, Convolution2D, MaxPooling2D, UpSampling2D
from keras.models import Model, Sequential
from keras.optimizers import Adam, RMSprop

from utils import *
from vgg16 import Vgg16                                                                                                                                                                                                                                                                                                                    



path=""



def conv1():
    model = Sequential([
            BatchNormalization(axis=1, input_shape=(3,224,224)),
            Convolution2D(32,3,3, activation='relu'),
            BatchNormalization(axis=1),
            MaxPooling2D((3,3)),
            Convolution2D(64,3,3, activation='relu'),
            BatchNormalization(axis=1),
            MaxPooling2D((3,3)),
            Convolution2D(128,3,3, activation='relu'),
            BatchNormalization(axis=1),
            MaxPooling2D((3,3)),
            Convolution2D(512,3,3, activation='relu'),
            BatchNormalization(axis=1),
            MaxPooling2D((3,3)),
            Flatten(),
            Dense(1024, activation='relu'),
            Dense(512, activation='relu'),
            Dense(256, activation='relu'),
            BatchNormalization(),
            Dense(2, activation='softmax')
        ])
    
 
    return model




model = conv1()



model.load_weights("weights.h5")




from keras.models import load_model
from keras.preprocessing import image
import numpy as np

# dimensions of our images
img_width, img_height =224,224


# print the classes, the images belong to



host = "192.168.1.101"
port = 5555   



while(1):
    print("")
    print("")
    print("#########DriveOye#########")
    print("Live feed is disable due to high latency using photo mode")
    print("#########DriveOye#########")
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect((host, port))
    name=input("Image name")
    img = image.load_img(name, target_size=(img_width, img_height))
    x = image.img_to_array(img)
    x = np.expand_dims(x, axis=0)




    images = np.vstack([x])
    classes = model.predict_classes(images, batch_size=10)
    if (classes[0]==1):
        print("Unsafe Driving")
        s.sendall(b'Unsafe Driving')
    else:
        print("Safe Driving")
        s.sendall(b'Safe Driving')




